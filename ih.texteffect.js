var texteffect = {};

texteffect.thinking = false;
texteffect.observer = false;
texteffect.can_observe = window.hasOwnProperty("MutationObserver");
texteffect.RanLastFrame = false;
texteffect.lastFrameTime = 0;

texteffect.debug = false;

texteffect.effects = {};

texteffect.effects.wavy = {
	// example wavy text effect
	think: function(el) {
		var baseline = Date.now() / 300;
		var sined = 0;
		var posY = 0;
		var posX = 0;
		$.each(el.find('.texteffect_char'), function(k,v) {
			v = $(v);
			baseline += .5;
			sined = Math.sin(baseline) + 1;
			posY = -1 * (sined * (v.height() / 4));
			posX = Math.cos(baseline);
			v.css('transform', 'translateX('+posX+'px) translateY('+posY+'px)');
		})
	}
};

texteffect.effects.fadeIn = {
	think: function(el) {
		$('.texteffect_char').not('.texteffect_char_handled').first().addClass('texteffect_char_handled');
	}
}

texteffect.log = function(output) {
	if (texteffect.hasOwnProperty('debug') && texteffect.debug !== false) {
		console.log(output);
	}
}

texteffect.think = function() {
	texteffect.thinking = true;

	// only think every other frame
	// for smoother animations, utilize CSS transitions.
	// additionally, requestanimationframe only runs when page is visible (usually)
	if (texteffect.RanLastFrame) {
		// we just thought, give us a moment.
		texteffect.RanLastFrame = false;
	} else {
		// called every other rendered frame

		// calculate time since last frame was rendered
		if (texteffect.lastFrameTime === 0) {
			texteffect.lastFrameTime = Date.now() - 16;
		}

		var lastFrameDelta = Date.now() - texteffect.lastFrameTime;

		$.each(Object.keys(texteffect.effects), function(k,v) {
			$('.texteffect_handled.texteffect_'+v).each(function(_,el) {
				// run the effect's think func
				texteffect.effects[v].think($(el), lastFrameDelta);
			});
		});

		// insert lastFrameTime as right now, right after the last effect runner was handled.
		texteffect.lastFrameTime = Date.now();

		// flip/flop RanLastFrame so we run at 30fps
		texteffect.RanLastFrame = true;
	}

	// requestanimationframe is more performant than settimeout or setinterval
	// will run in next animation frame
	// (we skip every other frame though for performance)
	window.requestAnimationFrame(texteffect.think);
}

texteffect.register = function() {
	// register all unhandled texteffect elements
	$('.texteffect').each(function() {

		// this is the effect the element wants used
		var desired_effect = $(this).attr('data-texteffect'); 

		if (!texteffect.effects.hasOwnProperty(desired_effect)) {
			// the effect we tried to use doesn't exist
			texteffect.log("Tried to use texteffect "+desired_effect+" but doesn't exist! skipping.")
			return true; // "continue" equivalent for jquery each
		};

		// loop through text nodes and wrap each text character
		texteffect.wrapTextNodes(this);

		// first, remove texteffect class so we never double-handle (reduces load when searching for unhandled)
		$(this).removeClass("texteffect");

		// second, add handled class (so we can still find it)
		$(this).addClass("texteffect_handled");

		// third, add the desired effect as part of a classname (faster than searching by data attr iirc)
		$(this).addClass("texteffect_"+desired_effect);

		texteffect.log("Registered "+desired_effect+" texteffect on an unhandled element.");
	});
}

texteffect.observe = function() {
	// listens for DOM changes using MutationObserver
	if (texteffect.observer !== false) {
		// we're already listening
		texteffect.log("Already listening for mutation changes");
		return;
	}

	// activate mutation observer
	// this is ugly for a reason
	// using setTimeout(fn,0) sticks our function call at the end of the current queue instead of blocking and running it right this instant
	// should result in better performance?
	texteffect.observer = new MutationObserver(function(){ setTimeout(function() {texteffect.log("Observed a mutation!"); texteffect.register();}, 0)} );

	// listen for all node deletions and additions in body (childList:true), and its descendents (subtree:true)
	texteffect.observer.observe(document.querySelector('body'), {childList: true, subtree: true});
	texteffect.log("Successfully observing for mutation events");
}

texteffect.start = function() {
	// it doesn't hurt (much) to run this a ton
	// register only handles unhandled 
	texteffect.register();

	// if we already started thinking,
	// don't try to think again.
	// (and that's a threat)
	if (texteffect.thinking) {
		return;
	}

	if (texteffect.can_observe) {
		texteffect.log("Starting mutation observer");
		texteffect.observe();
	} else {
		// sad, we can't use mutationobserver
		texteffect.log("MutationObserver not supported in this environment.");
	}

	texteffect.log("Starting think loop");
	texteffect.think();
}

texteffect.wrapTextNodes = function(el) {
	// wraps each character in a text node with a span having class texteffect_char
	// a little messy, but there's no other way to handle it with html/css working the way it does.
	// could be worse :) this is an edge case for HTML to handle (per-char attributes)
	$(el).contents().each(function(){
		// loop through all nodes in element
		if (this.nodeType === 1) {
			// nodeType 1 is an element node
			// recursively wrap text nodes down
			// TODO: skip elements with texteffect_char class
			texteffect.wrapTextNodes(this)
		} else if (this.nodeType === 3) {
			// nodeType 3 is text
			// we use replaceWith to turn it into an element node, and wrap all NON-WHITESPACE characters with spans.
			$(this).replaceWith(this.nodeValue.replace(/(\S)/g, '<span class="texteffect_char">$1</span>'));
		}
	});
}