## Adding stream views

Each stream view is defined in the `streams` object in index.html. The key (for example, `yt`) is what shows up in the sidebar. `embed` (needs to be an iframe) is the html that gets injected into the stream body. `title` is what shows up on hover.

Simply define the new stream like thus:

```
streams.ex = {embed: '<iframe src="https://google.com/"></iframe>', title: 'Google?'}
```

Will add a new button to the sidebar (with the letters `ex` in it).

## Linking to a specific stream

Simply link to `index.html#<stream id>`. So, for youtube, `index.html#yt`

EZPZ

## Notes

HLS might be broken? Couldn't get it working locally.

`ih.texteffect.js` is a generic texteffect library I wrote for another project of mine - it makes the loading text wavy :)

There's no mobile support - as in, it won't switch to having chat under video if the orientation appears to be portrait. if there's demand I can add it in. Realistically I imagine most people will just watch on youtube if they're on mobile.

`ld.html` and `hls.html` exist because I'm too lazy to make the stream switching code work on anything but iframes. I'm sure you know how corner cutting goes.